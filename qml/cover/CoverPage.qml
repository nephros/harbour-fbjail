import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    Image {
        source: "./background.png"
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
        width: parent.width * 4/5
        fillMode: Image.PreserveAspectFit
        opacity: 0.33
    }
    Text { id: fratz
        text: "fratzen"
        anchors.centerIn: parent
        font.pixelSize: Theme.fontSizeHuge
        horizontalAlignment: Text.AlignHCenter
        color: Theme.secondaryHighlightColor
    }
    Text {
        text: "jail"
        anchors.left: fratz.horizontalCenter
        anchors.top: fratz.verticalCenter
        //anchors.right: parent.right
        font.pixelSize: Theme.fontSizeHuge
        color: Theme.secondaryColor
    }

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
