import QtQuick 2.6
import Sailfish.Silica 1.0

Item {
    id: bi
    property alias icon: but.icon
    property alias label: lbl.text
    property alias button: but
    height: but.height
    width: Math.max ( but.width, lbl.width )
    IconButton {
        id: but
        anchors.centerIn: parent
        icon.source: bi.icon + "?" + (down ? Theme.highlightColor : Theme.primaryColor )
    }
    Label {
        id: lbl
        anchors.verticalCenter: but.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: Theme.secondaryHighlightColor
        font.pixelSize: Theme.fontSizeTiny
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
