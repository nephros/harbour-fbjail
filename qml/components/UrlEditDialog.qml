import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
    id: root
    property string what
    property alias title: head.title
    Column {
        width: parent.width

        DialogHeader{id: head}

        TextField{
            id: field
            text: root.what
            focus: true
            //validator: RegExpValidator { regExp: /^http.?\:\/{2}[a-zA-Z0-9_.:-]+\.facebook/gmi }
            inputMethodHints: Qt.ImhUrlCharactersOnly
            softwareInputPanelEnabled: focus
            EnterKey.onClicked: { focus = false }
            EnterKey.iconSource: "image://theme/icon-m-enter"
            rightItem: IconButton {
                onClicked: { field.text = "https://"; field.forceActiveFocus() }
                width: icon.width
                height: icon.height
                //icon.source: "image://theme/icon-m-input-clear"
                icon.source: "image://theme/icon-m-backspace-keypad"
                opacity: field.text.length > 0 ? 1.0 : Theme.opacityLow
            }
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Paste")
            icon.source: "image://theme/icon-m-clipboard"
            enabled: Clipboard.text.length > 0
            onClicked: {
                field.forceActiveFocus();
                field.selectAll();
                field.paste();
            }

        }
    }
    onDone: {
        console.debug("Dialog done");
        if (result == DialogResult.Accepted) {
            what = field.text;
            console.debug("Dialog Accepted");
        }
    }
}

// vim: expandtab sw=4 st=4 ts=4 filetype=javascript
