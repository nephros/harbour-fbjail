import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.WebView.Popups 1.0

ContextMenuInterface {
    id: popup
    width: (parent.width/5)*4
    height: (parent.height/5)*4
    anchors.centerIn: parent

    downloadsEnabled: true

    function show() {
        opacity = 1.0
    }

    Rectangle {
        anchors.fill: parent
        color: "blanchedalmond"
        Row {
            width: parent.width
            Button {
                id: acceptBtn
                anchors.horizontalCenter: message.horizontalCenter
                anchors.topMargin: Theme.paddingLarge
                text: popup.acceptText
                onClicked: { popup.accepted(); popup.visible = false }
            }
            Button {
                id: rejectBtn
                anchors.horizontalCenter: message.horizontalCenter
                anchors.topMargin: Theme.paddingLarge
                text: popup.cancelText
                onClicked: { popup.rejected(); popup.visible = false }
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
