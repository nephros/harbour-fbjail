/*
 *
 * ======== Database General ========
 *
 */
var dbname    = 'FratzenJailDB';
var dbversion = "1.0";

function getDB() {
    var db = LocalStorage.openDatabaseSync(dbname, dbversion, 'Local Storage', 10000,
        function(db) {
            createSchema(db);
            // specifying a creation callback causes version to be set to
            // nothing, resulting SQL database version mismatches on use.
            // So define, and change version on creation:
            // see https://stackoverflow.com/questions/23021373/qtquick-localstorage-database-version-mismatch-missing-version-attribute-in-i
            db = db.changeVersion("", dbversion);
        }
    );
    return db;
}

// ----------------------------------------------------------------

function createSchema(db) {
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS bookmarks ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT , link TEXT NOT NULL UNIQUE, icon TEXT, prefer TEXT);');
        tx.executeSql('CREATE TABLE IF NOT EXISTS bookmarkmodel (id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT);');
    })
}

/*
 *
 * ======== Models ========
 *
 */
function createModelTable(table) {
    var db = getDB();
    db.transaction(function(tx) {
        tx.executeSql('DROP TABLE IF EXISTS ' , table);
        tx.executeSql('CREATE TABLE IF NOT EXISTS ' , table , '(id INTEGER PRIMARY KEY, data TEXT);');
    })
}

// ----------------------------------------------------------------

function saveModel(table, model) {

    var db = getDB();

    db.transaction(function(tx) {
        var rs = tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='{" + table + "}';");
        if (rs.rows.length <= 0) {
            createModelTable(table);
        }
    });

    for(var i = 0; i < model.length; i++) {
        obj = model[i];

        db.transaction(function(tx) {
            console.debug("DB: saving: ", JSON.stringify(obj));
            try {
                var rs = tx.executeSql("INSERT INTO " , table , " VALUES (?,?);", [i, JSON.stringify(obj)]);
                console.debug("DB: inserted: ", rs.insertId);
            } catch (e) { console.warn("DB: Exception: ", e ); }
        })
    }
}

/*
 *
 * ======== Bookmarks ========
 *
 */
function saveBookmark(data) {
    //console.debug("DB: saving bookmark json: ", JSON.stringify(data));
    var db = getDB();
    var ret = false;
    db.transaction(function(tx) {
        try {
            //console.debug("DB: saving bookmark: ", data.name, data.link);
            var rs = tx.executeSql("INSERT INTO bookmarks VALUES (?,?,?,?,?);", [null, data.name, data.link, data.icon, data.prefer]);
            //console.debug("DB: inserted: ", rs.insertId);
            ret=true;
        } catch (e) { console.warn("DB: Exception: ", e ); ret=false;}
    })
    return ret;
}

// ----------------------------------------------------------------

function deleteBookmark(link) {
    var db = getDB();
    var ret = false;
    db.transaction(function(tx) {
        try {
            tx.executeSql("DELETE FROM bookmarks WHERE link LIKE '" + link + "';");
            ret=true;
        } catch (e) { console.warn("DB: Exception: ", e ); ret=false;}
    })
    return ret;
}

// ----------------------------------------------------------------

function updatePreference(what, link) {
    if (!what.matches(/[bmd]/i))
        return false;

    var ret = false;
    db.transaction(function(tx) {
        try {
            var rs = tx.executeSql("UPDATE bookmarks SET prefer = '" + what.toLowerCase() + "' WHERE link LIKE '" + link + "';");
            ret=true;
        } catch (e) { console.warn("DB: Exception: ", e ); ret=false;}
    })
    return ret;
}
// ----------------------------------------------------------------

function listBookmarks() {
    var db = getDB();
    var data = []
    db.readTransaction(function(tx) {
        try {
            var results = tx.executeSql('SELECT * FROM bookmarks;')
            for (var i = 0; i < results.rows.length; i++) {
                data[i] = results.rows.item(i);
            }
        } catch (e) { console.warn("DB: Exception: ", e ); }
    })
    //console.debug("DB: bookmark returned: ", JSON.stringify(data), null, 2);
    return data;
}

// ----------------------------------------------------------------


// vim: ft=javascript expandtab ts=4 sw=4 st=4
