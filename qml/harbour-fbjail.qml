import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
//import Nemo.Connectivity 1.0
import MeeGo.Connman 0.2
import "cover"
import "pages"
import "config/ua-strings.js" as UA

ApplicationWindow {
    id: app

    property alias online: onlineHelper.online
    property bool offline: !online

    property alias savedUrl: browser.savedUrl
    property alias hideui: config.hideui
    property alias remember: config.remember
    property alias mode: config.mode
    property alias nag: config.nag
    property alias chronoSort: config.chronoSort
    property alias advancedUA: browser.useAlternateMUA
    property alias zoom: browser.zoom
    property alias mem: browser.mem

    readonly property string simpleMUA:    UA.data.simpleUA     + " " + Qt.application.name + "/" + Qt.application.version;
    readonly property string alternateMUA: UA.data.alternateMUA + " " + Qt.application.name + "/" + Qt.application.version;
    readonly property string tabletUA:     UA.data.tabletUA     + " " + Qt.application.name + "/" + Qt.application.version;
    readonly property string fullUA:       UA.data.fullUA       + " " + Qt.application.name + "/" + Qt.application.version;

    readonly property string mobileUA: browser.useAlternateMUA ? alternateMUA : simpleMUA

    background.material: "glass"
    //background.color: "transparent"
    // real fb color, but doesn't come out the same:
    //background.material: "monochrome"
    //background.color: Qt.darker("#4267b2")

    DBusAdaptor {
        id: dbus

        bus: DBus.SessionBus
        service: 'org.nephros.sailfish.FratzenJail'
        iface: 'org.nephros.sailfish.FratzenJail'
        path: '/org/nephros/sailfish/FratzenJail'

        xml: '<interface name="org.nephros.sailfish.FratzenJail">
               <method name="openUrl">
                 <arg name="url" type="s" direction="in">
                   <doc:doc><doc:summary>url to open</doc:summary></doc:doc>
                 </arg>
               </method>
             </interface>'

        function openUrl(u) {
            console.log("openUrl called via DBus:" + u)
        }
    }

    Component.onCompleted: {
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
    }
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        property bool hideui: true
        property bool remember: true
        property string mode
        property bool nag: true
        property bool chronoSort: true
    }
    ConfigurationGroup  {
        id: browser
        scope: settings
        path:  "browser"
        property bool useAlternateMUA: false
        property int mem: -1
        property double zoom: 2.8
        //readonly property string simpleMUA: "Mozilla/5.0 (Sailfish 4.0; Mobile; rv:60.0) Gecko/60.0 Firefox/60.0 " + Qt.application.name + "/" + Qt.application.version
        //readonly property string alternateMUA: "Mozilla/5.0 (Maemo; Linux; U; Jolla; Sailfish; Android 2.3.5) AppleWebKit/538.1 (KHTML, like Gecko) Version/5.1 Chrome/30.0.0.0 Mobile Safari/538.1 (compatible)" + Qt.application.name + "/" + Qt.application.version
        ////readonly property string tabletUA: "Mozilla/5.0 (Sailfish 4.0; Tablet; rv:60.0) Gecko/60.0 Firefox/60.0 " + Qt.application.name + "/" + Qt.application.version
        //readonly property string fullUA:   "Mozilla/5.0 (Wayland; Linux; rv:60.0) Gecko/20100101 Firefox/60.0 " + Qt.application.name + "/" + Qt.application.version

        property string userUA: ""
        property string savedUrl: ""
        // TODO: obfuscate because privacy. atob/btoa are NOT available
        // maybe even put into encrypted SQL once we have it
        function saveUrl(u) { savedUrl = u; }
        function loadSavedUrl() { return Qt.resolvedUrl(savedUrl); }

    }
    //ConnectionHelper {
    //    id: onlineHelper
    //    readonly property bool online: status >= ConnectionHelper.Connected
    //}
    NetworkManager {
        id: onlineHelper
        readonly property bool online: state == "online" || state == "ready"
    }
    cover: Component { CoverPage{} }
    initialPage: Component { MainPage{} }
}

 // vim: ft=javascript expandtab ts=4 sw=4 st=4
