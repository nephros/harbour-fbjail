var fortunes = [
    "Why not delete your Facebook account today?",
    "Friends don't let friends use Facebook.",
    "You are not using Facebook, it is using you.",
    "Facebook knows more about you than you know about yourself.",
    "Remember: Facebook is bad for your mental health.",
    "Facebook deactivation reduces political polarization",
    "Have you considered going on a digital detox?",
    "Remember the Cambridge Analytica scandal? Look it up.",
    ""
];

function get() {
    var i = Math.floor((Math.random() * fortunes.length) );
    return fortunes[i];
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
