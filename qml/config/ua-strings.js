var data = {
    "simpleMUA":		"Mozilla/5.0 (Sailfish 4.0; Mobile; rv:60.0) Gecko/60.0 Firefox/60.0",
    "alternateMUA":		"Mozilla/5.0 (Maemo; Linux; U; Jolla; Sailfish; Android 2.3.5) AppleWebKit/538.1 (KHTML, like Gecko) Version/5.1 Chrome/30.0.0.0 Mobile Safari/538.1 (compatible)",
    "fullUA":			"Mozilla/5.0 (Wayland; Linux; rv:60.0) Gecko/20100101 Firefox/60.0",
    "tabletUA":			"Mozilla/5.0 (Sailfish 4.0; Tablet; rv:60.0) Gecko/60.0 Firefox/60.0"
}
