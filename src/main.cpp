/*
 *
 * Copyright 2022 Peter G. <sailfish@nephros.org>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
 * Minimal SailfishApp.
 *
 * The sole reason for this is to produce a binary, because the booster-browser
 * service will only launch binaries, not qmlscene, not sailfish-qml, etc.
 *
 */

/*
#include <QQuickView>
#include <QScopedPointer>
#include <QtQuick>
*/
#include <sailfishapp.h>

int main(int argc, char **argv) {
   return SailfishApp::main(argc,argv);
}
