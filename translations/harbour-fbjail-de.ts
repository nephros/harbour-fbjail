<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="18"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>What&apos;s %1</source>
        <translation>Was soll %1 sein</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>%1 is an unofficial Facebook client for Sailfish OS.</source>
        <translation>%1 is ein inoffizieller Client für Sailfish OS.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>It is in fact a simple combination of Sailfish OS features WebView and SailJail so your FB browsing is separated from your other browsing activities.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Hopefully this will improve your privacy a little bit - although of course Facebook/Meta are world-class in spying so additional measures are required for this to actually be effective.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>%1 is free software released under the MIT License.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>WebView and SailJail are developed by Jolla and the Sailfish OS community.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>WebView includes software developed by Mozilla and is licensed under the Mozilla Public License 2.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>SailJail is licensed under the 3-Clause BSD license and is based on Firejail developed by the NetBlue project licensed under the GPL 2.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Source Code</source>
        <translation>Quellcode</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>available on &lt;a href=&apos;https://gitlab.com/nephros/harbour-fbjail&apos;&gt;GitLab&lt;/a&gt;</source>
        <translation>verfübar auf &lt;a href=&apos;https://gitlab.com/nephros/harbour-fbjail&apos;&gt;GitLab&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Developers</source>
        <translation>Entwickler</translation>
    </message>
</context>
<context>
    <name>BarMenu</name>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="30"/>
        <source>Basic</source>
        <comment>page render mode</comment>
        <translation>Basis</translation>
    </message>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="42"/>
        <source>Mobile</source>
        <comment>page render mode</comment>
        <translation>Mobil</translation>
    </message>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="54"/>
        <source>Tablet</source>
        <comment>page render mode</comment>
        <translation>Tablet</translation>
    </message>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="64"/>
        <source>Desktop</source>
        <comment>page render mode</comment>
        <translation>Desktop</translation>
    </message>
</context>
<context>
    <name>BottomBar</name>
    <message>
        <location filename="../qml/components/BottomBar.qml" line="95"/>
        <source>Network offline</source>
        <translation>Nicht verbunden</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="33"/>
        <source>Basic</source>
        <comment>page display mode</comment>
        <translation>Basis</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="43"/>
        <source>Mobile</source>
        <comment>page display mode</comment>
        <translation>Mobil</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="53"/>
        <source>Tablet</source>
        <comment>page display mode</comment>
        <translation>Tablet</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>Desktop</source>
        <comment>page display mode</comment>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="156"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="157"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>RightPanel</name>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="55"/>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="72"/>
        <source>Bookmarks</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="93"/>
        <source>Load mobile</source>
        <translation>mobil laden</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="94"/>
        <source>Load desktop</source>
        <translation>voll laden</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="95"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="103"/>
        <source>Add</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="104"/>
        <source>Adding bookmark failed (duplicate link?)!</source>
        <translation>Hinzufügen fehlgeschlagen! (Duplikat?)</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="10"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="14"/>
        <source>Application</source>
        <translation>App</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="21"/>
        <source>Hide UI in landscape mode</source>
        <translation>UI im Landschaftsmodus verbergen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="22"/>
        <source>If enabled, the top and bottom bars are hidden in landscape mode.</source>
        <translation>Wenn aktiv verschwinden die Balken oben un dunten im Landschaftsmodus.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="39"/>
        <source>Remember last page</source>
        <translation>Seite merken</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="40"/>
        <source>If enabled, the last visited page will be loaded after application (re)start.</source>
        <translation>Wenn aktiv wird die letztbesuchte Seite gespeichert und beim Neustarten geladen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="46"/>
        <source>Facebook</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <source>Sort chronologically</source>
        <translation>Chronologisch ordnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="54"/>
        <source>If enabled, the order of posts will not be suggested by facebook, but will be chronological</source>
        <translation>Wenn aktiv werden die Beiträge nach Zeit, nicht wie von Facebook vorgeschlagen, sortiert.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="60"/>
        <source>Browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="67"/>
        <source>Use alternative UA string for mobile pages</source>
        <translation>Alternativen UA-Kenner für mobile Seiten verwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="68"/>
        <source>If enabled, a specialized User-Agent string is used to request mobile pages.</source>
        <translation>Wenn aktiv wird ein spezieller Auser-Agent Kenner beim Aufruf von mobilen Seiten verwendet.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="74"/>
        <source>Advanced</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="80"/>
        <source>Page Zoom factor</source>
        <translation>Seiten-Zoomfaktor</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="92"/>
        <source>Cache Memory</source>
        <translation>RAM Cache</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="99"/>
        <source>automatic</source>
        <translation>automatisch</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="101"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="115"/>
        <source>Restart the App to apply changes to zoom factor or mem cache.</source>
        <translation>Neu starten um Änderungen an Cache oder Zoom anzuwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="30"/>
        <source>Show healthy reminders</source>
        <translation>Praktische Hinweise anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="31"/>
        <source>If enabled, some parts of the UI will show helpful reminders about Facebook usage.</source>
        <translation>Wenn aktiv werden Hinweise zum gesunden Gebrauch von Facebook angezeigt.</translation>
    </message>
    <message>
        <source>Restart the App to apply zoom factor changes.</source>
        <translation type="vanished">Zum Anwenden von Änderungen beim Zoomfaktor App neu starten.</translation>
    </message>
</context>
<context>
    <name>TopBar</name>
    <message>
        <location filename="../qml/components/TopBar.qml" line="7"/>
        <source>Loading...</source>
        <translation>Wird geladen...</translation>
    </message>
    <message>
        <location filename="../qml/components/TopBar.qml" line="63"/>
        <source>URL copied to Clipboard!</source>
        <translation>URL in die Zwischenablage kopiert!</translation>
    </message>
    <message>
        <location filename="../qml/components/TopBar.qml" line="72"/>
        <source>Edit Url</source>
        <translation>URL editieren</translation>
    </message>
</context>
<context>
    <name>UrlEditDialog</name>
    <message>
        <location filename="../qml/components/UrlEditDialog.qml" line="33"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
</context>
</TS>
